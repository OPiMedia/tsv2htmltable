.. -*- restructuredtext -*-

=============
tsv2htmltable
=============
Program (and Python **3** library)
to easily **convert** text data as **TSV, CSV**…
**to fancy HTML tables** that use DataTables_ (or LaTeX or other text data),
with a lot of options.

.. _DataTables: https://www.datatables.net/

Simple example invocation

.. code-block:: bash

  tsv2htmltable --dest-html-css-inlined-default --dest-html-js-inlined-default -o OUTPUT.html INPUT.tsv

to produced ``OUTPUT.html``

|abcd example|

from ``INPUT.tsv`` like this TSV data:

.. code-block:: text

  A	B	C	D
  1	2	3,14	4
  5	6	7
  9	10	11	12


Some real examples of use:
`round of 0.5 and Euclidean division comparisons between some programming languages`_
and
`HTML results pages of Scala-Par-AM benchmarks`_

.. |abcd example| image:: https://bitbucket.org/OPiMedia/tsv2htmltable/raw/6c6b16cfd9cc94cf013d41ef660aac22f7dc1b1f/_img/tsv2htmltable--simple-example.png

.. _`HTML results pages of Scala-Par-AM benchmarks`: http://www.opimedia.be/CV/2017-2018-ULB/MEMO-F524-Masters-thesis/benchmark-results/
.. _`round of 0.5 and Euclidean division comparisons between some programming languages`: http://www.opimedia.be/DS/languages/comparative/divisions/divisions.html

|



Installation
============
**Work in progress…**

**This repository in empty for now.**

|



Links
=====
**Work in progress…**

|



Other links maybe useful
------------------------
* `CSV compare`_: Web application to compare two TSV (CSV)
* `Tables Generator`_: Web application to generate LaTeX, HTML…

.. _`CSV compare`: https://extendsclass.com/csv-diff.html
.. _`Tables Generator`: https://www.tablesgenerator.com/

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2018, 2019 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png



|tsv2htmltable|

.. |tsv2htmltable| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/15/1591063001-2-tsv2htmltable-logo_avatar.png
